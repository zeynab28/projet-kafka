package com.atos.labKafka.type;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Trafic {
	
	String marque;
	@JsonProperty("matricule")
	String matricule;

	String heure;
	int limite;
	Date date;
	public Trafic(String marque, String matricule, String heure, int limite, Date date) {
		super();
		this.marque = marque;
		this.matricule = matricule;
		this.heure = heure;
		this.limite = limite;
		this.date = date;
	}
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	public String getHeure() {
		return heure;
	}
	public void setHeure(String heure) {
		this.heure = heure;
	}
	public int getLimite() {
		return limite;
	}
	public void setLimite(int limite) {
		this.limite = limite;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Trafic() {
		
	}
	@Override
	public String toString() {
		return "Trafic [marque=" + marque + ", matricule=" + matricule + ", heure=" + heure + ", limite=" + limite
				+ ", date=" + date + "]";
	}
	
	

}
