package com.atos.labKafka.serde;




import com.atos.labKafka.type.*;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

import java.util.HashMap;
import java.util.Map;

public class AppSerDes extends Serdes {

    static public final class TraficSerde extends WrapperSerde<Trafic> {
        public TraficSerde() {
            super(new JsonSerializer<>(), new JsonDeserializer<>());
        }
    }

    static public Serde<Trafic> Trafic() {
        TraficSerde serde =  new TraficSerde();

        Map<String, Object> serdeConfigs = new HashMap<>();
        serdeConfigs.put(JsonDeserializer.VALUE_CLASS_NAME_CONFIG, Trafic.class);
        serde.configure(serdeConfigs, false);

        return serde;
    }

   

	


}