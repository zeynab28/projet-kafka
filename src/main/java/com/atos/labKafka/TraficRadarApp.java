package com.atos.labKafka;

import com.atos.labKafka.serde.*;

import com.atos.labKafka.type.*;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;


import java.util.Properties;

public class TraficRadarApp {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "trafic-dakar-application");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
//        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String());
//        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.Trafic());
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);

        final StreamsBuilder builder = new StreamsBuilder();
		

        KStream<String, Trafic> source = builder.stream(
            "ks.cars.input",
            Consumed.with(AppSerDes.String(), AppSerDes.Trafic())
        );

        source
            .to("ks.cars.output", Produced.with(AppSerDes.String(), AppSerDes.Trafic()));

       

     
       
        source
            .filter((k, v) -> v.getLimite() > 3*TraficRadarConfigs.limite)
            .peek((k, v) -> System.out.println("Key = " + k + " Value = " + v))
            .to("ks.cars.input", Produced.with(AppSerDes.String(), AppSerDes.Trafic()));

           }
}
