package com.atos.labKafka;



public class TraficRadarConfigs {
    public static final String appName = "trafic-Dakar";
    public static final String brokers = "localhost:9092";
    public static final String storeName = "mt-kv-store";
    public static final String inputTopic = "ks.cars.input";
    public static final String outputTopic = "ks.cars.output";
    public static final int limite = 300;
}
