package com.atos.labKafka;

import com.atos.labKafka.serde.*;
import com.atos.labKafka.type.*;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class StreamApp {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, TraficRadarConfigs.appName);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, TraficRadarConfigs.brokers);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);

        final StreamsBuilder builder = new StreamsBuilder();

        KStream<String, Trafic> source = builder.stream(
        	TraficRadarConfigs.inputTopic,
            Consumed.with(
                Serdes.String(),
                AppSerDes.Trafic()
            )
        );

        source.filter((k, v ) -> v.getLimite() > 3*TraficRadarConfigs.limite, Named.as("high-limite"))
              .peek((k, v) -> System.out.println("Key = " + k + " Value = " + v))
              .to(TraficRadarConfigs.outputTopic, Produced.with(AppSerDes.String(), AppSerDes.Trafic()));

        final Topology topology = builder.build();

        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        System.out.println("Starting Kafka Streams application...");
        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }

        System.exit(0);
    }
}
